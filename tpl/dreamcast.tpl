<!DOCTYPE html>
<!-- vim: set sw=2: -->
<html>
  <head>
    <meta charset="UTF-8">
    <title><?theme title?></title>
    <meta property="og:site_name" content="rnd's website"/>
    <meta property="og:title" content="<?theme title?>"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://rnd.neocities.org/<?theme dir?><?theme source?>"/>
    <link rel="icon" type="image/png" href="favicon.png">
    <?theme style?>
  </head>
  <body x-margintop="0" x-marginleft="0" topmargin="0" marginwidth="0" marginheight="0" leftmargin="0" bgcolor="#050403">
    <table align="center" width="640" cellspacing="0" cellpadding="0" border="0"><tbody>
    <tr bgcolor="#050403"><td width="256px"><a href="/dc"><img border="0" align="top" src="/website_banner.gif" width="256" height="32" alt="rnd's website logo"></a><font color="#fefefe"></td><td align="center" valign="middle" background="/website_banner_bg.gif"><font color="#fefefe"><?theme title?></font></td></tr>
    <tr height="32" bgcolor="#221f31"><td colspan="2" background="/menu_background.gif">
      	<center><nav class="links">
	  <a href="/dc/index.html"><img align="top" border="0" width="128" height="32" src="/menu_button_main.gif" alt="Main page"></a><a href="/dc/about_me.html"><img align="top" border="0" width="128" height="32" src="/menu_button_about.gif" alt="About me"></a><a href="/dc/blog"><img align="top" border="0" width="128" height="32" src="/menu_button_blog.gif" alt="Blog"></a><a href="/dc/tokipona"><img align="top" border="0" width="128" height="32" src="/menu_button_tp.gif" alt="toki pona"></a>
	</nav></center>
    </td></tr>
    <tr bgcolor="#fefefe"><td colspan="2">
      <article class="content">
        <form name="content">
	  <?theme body?>
	</form>
      </article>
    </td></tr>
    <tr><td colspan="2" bgcolor="#050403">
      <font color="#fefefe">
	<a href="https://neocities.org/"><img src="/banners/neocities.gif"
	width="88" height="31" alt="Hosted by Neocities" /></a>
	<a rel="me" href="https://cybre.space/@devurandom"><img src="/banners/mastodon.gif"
	width="88" height="31" alt="Follow me on Mastodon" /></a>
	<a href="https://www.vim.org"><img src="/banners/vim.gif"
	width="88" height="31" alt="This page made with Vim" /></a>
	<a href="https://twitter.com/AdigunPolack/status/978086591485882368"><img src="/banners/simplejpc16.gif"
	width="88" height="31" alt="SimpleJPC-16" title="This website uses the SimpleJPC-16 palette for its design." /></a>
	<a href="https://mozilla.org/en-US/firefox/"><img src="/banners/firefox.gif"
	width="88" height="31" alt="Best viewed in Firefox" title="This website is best viewed in the Firefox web browser."/ ></a>
	<a href="https://alpinelinux.org"><img src="/banners/alpine.gif"
	width="88" height="31" alt="Alpine Linux"/ ></a>
	<a href="https://www.thedreamcastjunkyard.co.uk/"><img src="/banners/dcjy.gif"
	width="88" height="31" alt="The Dreamcast Junkyard"/ ></a>
	<a href="https://dreamcastlive.net/"><img src="/banners/dclive.gif"
	width="88" height="31" alt="Dreamcast Live"/ ></a>
	<a href="https://blaseball.com"><img src="/banners/blaseball.gif"
	width="88" height="31" alt="Blaseball"/ ></a>
	<a href="https://f-droid.org"><img src="/banners/fdroid.gif"
	width="88" height="31" alt="F-Droid" title="F-Droid is a repository of free and open source Android applications."/ ></a>
	<a href="https://rnd.neocities.org/tokipona/"><img src="/banners/mute_lukin.gif"
	width="88" height="31" alt="jan li lukin e lipu ni lon tenpo mute"/ ></a>
	<a href="https://jetsetradio.live/"><img src="/banners/jsrl.gif"
	width="88" height="31" alt="Jet Set Radio Live"/ ></a>
	<br />
	Website created and updated by /dev/urandom. Source code available on the 
	<a href="https://gitlab.com/dev_urandom/simple-site">GitLab page</a>.
      </font>
    </td></tr>
    </tbody></table>
  </body>
  <script src="/dc_scripts.js"></script>
</html>

