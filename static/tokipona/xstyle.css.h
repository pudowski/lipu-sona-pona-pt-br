.header {
  background: #f5ee9b;
  color: #404a68;
}

@media screen {

	.header_image {
		padding-top: 12.5%;
		background-image: url("tokipona_banner.gif");
		background-size: contain;
		background-repeat: no-repeat;
		background-position: center;	
		image-rendering: pixelated;
		image-rendering: crisp-edges;
	}
	
	.header_text {
		background: rgba(245,238,155,0.75);
	}

}


