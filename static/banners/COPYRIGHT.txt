Files in this directory include copyrighted contents that belong to other people
and companies and therefore can not be released under the terms of the MIT
license.

alpine.gif: © 2020 Alpine Linux Development Team

blaseball.gif: © 2020 The Game Band

cdlive.gif: © Dreamcast Live

dcjy.gif: © The Dreamcast Junkyard, SEGA Corp.

fdroid.gif: © F-Droid Limited and Contributors

firefox.gif: © Mozilla Foundation, Mozilla Corporation

jsrl.gif: © Jet Set Radio Live, SEGA Corp.

mastodon.gif: © Jin Nguyen

neocities.gif: © Neocities

pso.gif: © Sonic Team, SEGA Corp.

vim.gif: © Bram Moolenaar and contributors
