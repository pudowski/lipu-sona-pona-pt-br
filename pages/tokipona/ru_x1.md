% токи пона - дополнительная страница 1 - старые и новые слова
% /dev/urandom
% april 2020

Официальный словарь токи пона содержит 120 слов, описанных на страницах с первой
по двенадцатую этого курса. Но в сообществе токи пона также используются
дополнительные слова, которые либо были созданы ранее и удалены из официальной
книги, либо были придуманы самим сообществом. В отличие от "неофициальных слов",
которые используются для имён собственных (см. [страницу 7](ru_7.html)), эти
слова считаются родными словами токи пона и не пишутся с заглавной буквы.

Документ ["nimi ale pona"][nap] содержит более-менее полный список всех слов в
токи пона.

[nap]:https://docs.google.com/document/d/10hP3kR7mFN0E6xW3U6fZyDf7xKEEvxssM96qLq4E0ms/edit

Но на этой странице будут описаны только те слова, которые, с моей точки зрения,
достаточно часто используются самим сообществом.

Поскольку основной идеей токи пона является сокращение словарного запаса,
практически все эти слова вызывают споры по поводу того, должны ли они
существовать. На этой странице я буду высказывать свои мнения о необходимости
этих слов.

## Совмещённые слова: kin, namako и oko

До публикации официальной книги, в словаре языка были некоторые
часто-используемые слова, смысл которых был слишком близок к другим словам.
Вместо того, чтобы полностью от них избавиться, они стали синонимами других
слов.

Слово "kin" описывается, как синоним слову "a", но если "a" служит, как
эмоциональное междометие, слово "kin" использовалось, как способ выразить
значимость предложения ("действительно", "реально", "очень"). По моему мнению,
этот смысл можно отлично выразить словами "a" или "mute" ("очень").

Слово "namako" обозначало "дополнение" или "приправа", но в официальной книге
стало синонимом слову "sin" ("новый", "дополнительный"). Хотя значения этих двух
слов немного отличаются, я считаю, что слово "sin", в особенности как
существительное в фразе "sin moku" (дополнение к еде), отлично подходит для
выражения той же идеи.

Слово "oko" записано, как синоним к слову "lukin". До этого, "oko" обозначало
"глаз", "око", а "lukin" -- "зрение","взгляд","смотреть". По аналогии с "kute",
которое можно использовать и как "слух", и как "слушать", и как "ухо",
совмещение этих двух слов вполне разумно.

## Удалённые слова

Некоторым словам не так сильно повезло. Хотя они использовались в сообществе до
публикации официальной книги, в официальный словарь они вообще не попали.

Начнём с более часто-используемых слов:

Слово "apeja" значит "стыд" или "вина". Описать идею стыда или вины, используя
только слова из официальной книги, довольно сложно, и поэтому это слово до сих
пор часто используется.

Слово "kipisi" имеет значения "резать, делить, кусок". Некоторые из этих
значений сейчас принадлежат словам "tu" (делить) и "wan" (часть, элемент), но в
некоторых текстах оно всё ещё используется. Даже существуют предложения для
символов этого слова в иероглифической системе *sitelen pona*.

Слово "leko" (которое, скорее всего, произошло от названия бренда Lego) значит
"блок", "квадрат" или иногда "ступеньки". Простой замены этому слову нет,
поэтому его до сих пор время от времени используют.

Слово "monsuta" значит "монстр" или "страх". Как и с "apeja", просто так описать
смысл этого слова довольно сложно (плюс, у разных людей разные представления о
страхе), и поэтому старое слово до сих пор используется.

А вот слова, которые почти не используются в современности, но встречаются в
старых текстах:

Слово "kapa" значило "гора, холм". Сейчас его заменило слово "nena".

Слово "kapesi" было ещё одним цветовым термином, описывающим серый и коричневый
цвета (и иногда используемым в значении "кофе"), но оно было удалено, так как
описать эти цвета спокойно можно фразами "pimeja walo" и "pimeja jelo".

Слово "majuna" значило "старый" и также оказалось удалено из словаря. Его смысл
можно передать другими фразами имеющими отношение к времени:

> ona mute li majuna. -- Они старые.

> tenpo mute la ona mute li lon. -- Они существовали очень долго.

Слово "pasila" обозначало "простой", но его очень рано совместили с "pona".

Слово "pake", от английского "block", обозначало "остановить, прекратить", но
оно было удалено, так как его смысл можно передать либо с "pini" (остановить,
закончить), либо с "awen" (держать, оставаться).

Слово "pata" значит "брат" или "сестра", но его смысл щас передаётся как "jan
sama".

Слово "powe" значит "ложь" или "ложный". Сейчас его значение обычно передаётся
фразами, содержащими "lon ala" ("не существует") или "sona ike" ("плохое знание,
некорректная информация").

Также существовали слова "tuli" и "po", как числительные 3 и 4. Вместо них
сейчас используются фразы "tu wan" и "tu tu".

## Направления

В токи пона есть слова "вверху", "внизу", "впереди" и "сзади", но нет слов
"слева" или "справа", только одно общее слово для "сбоку".

Самый простой способ описать эти направления -- это использовать тот факт, что
большинство людей пишут правой рукой ("poka pi luka sitelen" = правый, "poka pi
luka sitelen ala" = левый), что их сердце находится на левой половине тела
("poka pilin" = левый, "poka pilin ala" = правый) или пишут слева направо.
("poka open" = левый, "poka pini" = правый).

Но все эти описания не на 100% правильны: есть люди, которые пишут левой рукой,
у которых сердце расположено справа или чья письменность направлена справа
налево. (Хотя все основные письменности *для токи пона* -- латиница, sitelen
pona и sitelen sitelen -- пишутся слева направо.)

В документе "nimi ale pona" описываются два "после-pu"шных слова с конкретными
значениями: "soto" значит "лево", а "te" -- "право". Я считаю, что эти слова
могут быть необходимыми в некоторых случаях, но лучше всего при возможности их
избежать.

## Пол и сексуальная ориентация

> %warning%
> Если вы живёте в стране, где правительство считает, что информация о
> не-гетеросексуальных людях является "пропагандой" и не должна быть доступной
> для несовершеннолетних, то не нажимайте на кнопку "Раскрыть раздел", если вам
> меньше 18 лет.
>
> Если вы согласны с позицией этого правительства, то закройте эту страницу,
> выключите ваш компьютер или мобильное устройство и вернитесь обратно в 20-й
> век.
>
[Раздел о словах, относящихся к сообществу ЛГБТ](ru_x1_lgbt.html)

## Шуточные слова

Некоторые из "дополнительных" слов токи пона были созданы как шутки самой Соней
Лэнг. В документе "nimi ale pona", они помечены как "w.o.g. Sonja". Самое
популярное из этих слов, "kijetesantakalu", обозначает "енот" или "представитель
семейства енотовых".

Также шуточные слова включают в себя "mulapisu" ("пицца") и "yupekosi"
("изменять свои старые творения в худшую сторону") -- стоит заметить, что в токи
пона не используется буква "y" и произношение этого слова неизвестно.

## Таблица самых часто-используемых дополнительных слов

Этот список был создан на основе [таблицы частоты использования дополнительных 
слов](https://docs.google.com/spreadsheets/d/1dGd4do1Jk2L2NwW5l7tLgSajAVkUqO0z2UHGu4_Sq_M)
за авторством пользователя Reddit `qwertyter`, с дополнительными изменениями на
основе моего личного опыта.

В столбце "альтернативы" указаны слова и фразы из официального словаря, которыми
можно выразить схожие идеи. Не все из них подходят полностью, и для некоторых из
них нужно использовать другие части речи (например, "tu" как прилагательное в
значении "разделённый" без частицы "li" легко спутать с числом "два".)

| слово   | значение                           | альтернативы          |
|---------|------------------------------------|-----------------------|
| kin     | эмфаза ("очень", "тоже")           | a, mute               |
| monsuta | страх, монстр                      |                       |
| lanpan  | получить, взять, брать             | kama jo (kama jo utala) |
| oko     | глаз                               | lukin                 |
| tonsi   | небинарный / трансгендерный, ЛГБТ  |                       |
| kipisi  | делить, резать                     | tu                    |
| namako  | дополнительный, приправа           | sin                   |
| kijetesantakalu | енот                       |                       |
| leko    | квадрат, кирпич, ступени           |                       |
| powe    | ложь, неправда                     | sona ike, ... li lon ala |
| apeja   | стыд, позор, вина                  |                       |
| majuna  | старый                             | pi tenpo mute pini    |
| pake    | прекращать, блокировать            | pini, pali ala        |
| linluwi | интернет                           |                       |

[Главная страница](ru_index.html)
