% toki pona page 7 - interjeições, perguntas, comandos e nomes
% /dev/urandom
% março de 2020

O vocabulário dessa página:

| palavra | significado                      |
|---------|----------------------------------|
| a       | (interjeição emocional)          |
| anu     | ou                               |
| mu      | (som de animal)                  |
| o       | se dirigindo à pessoas, comandos)|
| seme    | o que? (para perguntas)          |

| palavra | significado                                         |
|---------|-----------------------------------------------------|
| kute  | escutar, ouvir, obedecer, orelha                      |
| nimi  |  palavra, nome                                        |
| lawa  | cabeça, controlar, dominar, possuir, comandar         |
| anpa  | humilde, dependente, conquistar/derrotar, se curvar à |
| insa  | dentro, conteúdos, centro, estômago                   |

## Notas de vocabulário

Os significados diferentes de "anpa" parecem mutualmente excludentes, 
mas o significado real depende de que palavra segue depois.

Se "anpa" é usada como verbo com "e" e um objeto junto, então "anpa" 
significa "conquistar" ou "derrotar":

* jan wawa li anpa e jan utala ike. -- A pessoa forte derrotou o guerreiro do mal.

Porém, se "anpa" é usado sem objeto, ou com uma preposição como "tawa", 
ela significa "se curvar à"

* jan pali li anpa tawa jan lawa. -- O trabalhador se curvou diante ao chefe.

O trabalhador se curvou diante ao chefe.

> ona li pona mute anu ike mute. mi sona ala. -- Isso é ou muito bom ou muito ruim. Eu não sei.

## Interjeições e comandos

A palavra “a” funciona como uma interjeição emocional, usada para enfatizar ou 
adicionar emoção à frase. Ela é geralmente adicionada no final da frase ou 
funciona como uma frase própria. 

> sina suwi a! --  Você é tão fofo!

Mais especificamente, risada é indicado com a frase “a a a!” (ha ha ha!).

A palavra “mu” substitui qualquer som feito por qualquer animal.

A palavra “o” é usada para se referir à pessoas e dar comandos.

Quando usada sozinha no começo de uma frase, ela transforma o resto da 
frase em um comando.

> o kute e mi! -- Me escute!

Quando usada após uma frase que funciona como substantivo, ela é direcionada a uma pessoa.

> sina o! -- Ei, você!

> jan ale o! -- Todo mundo!

Ambos usos podem ser combinados.

> jan pali o, kepeken e ilo awen! -- Trabalhador, use equipamento de proteção!

## Perguntas

Há duas maneiras de fazer perguntas em toki pona.

Se você quer fazer uma pergunta de sim ou não, você fala a frase normalmente, 
mas substitui a palavra sendo questionada com uma estrutura “[palavra] ala [palavra]”.

> sina pona ala pona? -- Você tá bem?

Não há palavras para "sim" e "não", então para responder positivamente, você
repete a palavra sendo usada, e para responder negativamente, você adciona
"ala".

> pona. -- Sim.

> pona ala. -- Não.

(Pelo que eu entendo, essa estrutura é similar à usada em Mandarim.)

> ona li pali ala pali? -- Eles estão trabalhando?

> jan lili li moku ala moku? -- As crianças estão comendo?

Alternativamente, você pode adicionar "anu seme" ("ou o que?") no final da
frase.

> sina pona anu seme? -- Você tá bem?

Para perguntas de formato livre, você começa com uma frase comum e insere "seme"
na parte que você quer perguntar:

> sina pali e seme? -- No que você está trabalhando? ("Você trabalha no quê?")

> jan seme li pakala e ona? -- Quem quebrou? ("Qual pessoa quebrou?")

> ijo ni li seme? -- O que é essa coisa? ("Essa coisa é o que?")

> sewi li laso tan seme? -- Por que o céu é azul? ("Céu é azul por causa de o quê?")

## Nomes (palavras não-oficiais)

Até agora, essas páginas dependeram somente de palavras nativas de toki pona para
se referir a coisas e pessoas. Mas isso claramente não é o suficiente quando você
precisa chamar alguém pelo seu nome. Para substativos próprios, toki pona usa 
"palavras não-oficiais". Essas são geralmente nomes de pessoas, cidades, países,
etc., tomadas de suas línguas nativas e adaptadas às regras de pronúncia de toki
pona. Ao contrário de todas outras palavras de toki pona, elas são escritas com
a primeira letra maíuscula.

Palavras não oficiais sempre são tratadas como adjetivos, o que significa que 
antes dela sempre há um substantivo ou frase agindo como substantivo que descreve
ao que o nome se refere.

> jan Mimi -- (a pessoa) Mimi

> ma Kanata -- (o país) Canadá

> toki Inli -- (a língua) Inglês

> ma tomo Napoli -- (a cidade) Nápoles

Alternativamente, as palavras não oficiais podem ser usadas como adjetivos comuns:

> jan Kanata -- uma pessoa canadense

Já que há múltiplas maneiras de ligar nomes nativos à sons de toki pona, podem
haver múltiplos nomes não-oficiais para a mesma cidade ou nome de país. (Embora
hajam dicionários que as pessoas podem usar que incluem listas de nomes em toki 
pona para países, cidades e línguas.)

Pessoas falando toki pona são livres para escolher seus próprios nomes toki pona,
adaptando o nome de sua língua nativa ou inventando algo novo.

> %info%
> Como você pode ter notado, nomes pessoais são prefixados com "jan". Pessoas
> na comunidade de toki pona podem se referir a si mesmos com seu nome de toki
> pona mesmo quando usando outras línguas, adicionando "jan" no começo.

> %info%
> A [página 7a](ptbr_7a.html) contém mais informações sobre como palavras 
> não-oficiais são criadas.

> %warning%
> Enquanto essa não é a opção mais correta, não há problemas na maioria dos casos
> em não usar palavras não-oficiais e simplesmente pronunciar ou escrever o nome
> como você o faria em sua língua nativa. Por exemplo, você pode se referir à
> uma pessoa chamada Robert como "jan Lope" _ou_ "jan Robert".

## Exemplos

> o toki ala a! -- Cala boca! ("Não fale!")

> sina pali ala pali e ni? -- Você fez isso?

> mi jan San. mi lon ma Mewika. -- Eu sou John. Eu moro nos Estados Unidos.

> nimi sina li seme? -- Qual é seu nome?

> jan lawa mi li ike mute. -- (Meu chefe/Nosso líder) é muito ruim.

> jan Lopin o, toki! -- Olá, Robin!

## Exercícios

Agora, tente entender o significado dessas frases.

* jan Lisa o, moku ala e kili ni a!
* kulupu Kensa li anpa e kulupu ale ante.
* o toki insa ala e ni: jan pali li anpa tawa jan lawa.
* sina pali e ni tan seme? 
* insa mi li pakala. o pona e mi a!

E tente traduzir as seguintes frases para toki pona.

* Eu não acho que deuses existam.
* Não faça barulho na biblioteca.
* Meu chefe me diz pra não dormir no escritório.
* Seu irmão se parece muito com você.
* Não vá lá fora.

[Respostas](ptbr_answers.html#p7)

[Página anterior](ptbr_6.html) [Página inicial](ptbr_index.html) [Próxima página](ptbr_8.html)
