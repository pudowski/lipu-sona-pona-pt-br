% toki pona página 0 - escrita e pronúncia
% /dev/urandom
% março 2020

A língua *toki pona* usa somente 14 letras do alfabeto latino, e todas essas 
letras possuem pronúncias consistentes.

Essas são: a,e,i,j,k,l,m,n,o,p,s,t,u,w.

Você pode ter percebido que todas essas letras são minúsculas. Isso é porque 
todas as palavras em toki pona são em letras minúsculas, até no começo de 
frases.

As consoantes (j,k,l,m,n,p,s,t,w) usam os mesmos sons que o português, com 
exceção do "j", que soa como um "i" ou "y" ("jan" é lido como "ian", ou 
"yan"). A consoante "w" não é usada na língua portuguesa mas tem som de "u" 
("wawa" é lido como "uaua").

As vogais (a, e, i, o, u) são pronunciadas de formas muito parecidas com o 
português.

* **a** soa como "a".

* **e** soa como "é".

* **i** soa como "i".

* **o** soa como "ó".

* **u** soa como "u".

> %info%
> O Alfabeto fonético internacional (AFI) é uma maneira comum de escrever
> pronuncias específicas de frases e palavras em qualquer língua. 
>
> Ele usa uma versão do alfabeto latino com vários caracteres adicionais.
> Por exemplo, a pronuncia AFI de "Brasília" é /brəˈzɪliə/, mas, em toki
> pona, a pronúncia de cada letra é o seu próprio símbolo AFI! Então
> "toki pona" é pronunciado /toki pona/.

Já que há tantos poucos sons, o jeito que eles podem ser pronunciados é 
bastante flexível. Por exemplo, alguns podem substituir os sons "p, t, k" 
com "b, d, g". Tal mudança causaria muita ambiguidade e confusão em outras 
línguas, mas os sons de toki pona foram escolhidos para serem comuns em 
muitas outras línguas e fáceis de distinguir.

Todas as palavras em toki pona são pronunciadas com a sílaba tônica sendo 
a primeira da palavra.

## Exercícios 

Aqui estão algumas palavras em toki pona que são derivadas de, ou soam
similares à palavras em português:

| toki pona | soa similar à     |
|-----------|-------------------|
| nimi      | nome              |
| oko       | olho              |
| kulupu    | grupo             |
| linja     | linha             |
| tenpo     | tempo             |
| mi        | meu               |
| pan       | pão               |
| mute      | muito             |

[Página inicial](ptbr_index.html) [Próxima página](ptbr_1.html)

---

Enquanto eu tentava criar um curso não-oficial, eu acabei fazendo o conteúdo
dessa página ser similar àquele no [**livro oficial da Sonja Lang**](https://tokipona.org/).
Eu recomendo que todos dêem uma olhada nele.
