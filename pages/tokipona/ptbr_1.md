% toki pona página 1 - frases básicas
% /dev/urandom
% março 2020

O vocabulário desta página:

| palavra |significado                        |
|---------|-----------------------------------|
| mi      | eu, nós                           |
| sina    | tu, você, vós                     |
| ona     | ele, ela, eles, elas              |
| li      | (separa sujeito do predicado)     |
| pona    | bom, simples, melhorar, consertar |
| ike     | mau, ruim, complexo, desnecessário|
| suli    | grande, importante, crescer       |
| lili    | pequeno, pouco, jovem, encolher   |
| kili    | fruta, vegetal, cogumelo          |
| soweli  | animal, mamífero terrestre        |

Vamos começar com a estrutura de sentença mais básica em toki 
pona:

> [substantivo] li [substantivo/adjetivo]. 

Em Português, isso significa:

> [substantivo] é (um/a) [substantivo]. 

ou 

> [substantivo] é [adjetivo]. 

Por exemplo:

>  ona li suli. - [ele(s)/ela(s)] é/são [grande(s)/importante(s)]. 

Como é possível ver, uma única palavra pode ter múltiplos significados 
relacionados. No uso comum, tanto "ona" quanto "suli" serão mais claros com 
base no contexto.

> kili li pona. - [fruta(s)/vegetal(is)/cogumelo(s)] é/são [bom/bons]. 

E nesse caso, não faz muito sentido usar qualquer outro significado de "pona" 
além de "bom".

Há uma exceção à regra. Se o sujeito é "mi" ou "sina", não é necessário 
adicionar "li". Então, ao invés de

> sina li suli. - Você é importante. 

usamos

> sina suli. - Você é importante. 

## Exercícios

Como você expressaria essas ideias em toki pona?

*  Animais são importantes. 
*  Ele é pequeno. 
*  Eu sou grande. 
*  É um cachorro. 
*  Você é malvado.

[Respostas](ptbr_answers.html#p1)

[Página anterior](ptbr_0.html) [Página inicial](ptbr_index.html) [Próxima página](ptbr_2.html)
