% toki pona page 5 - isso e aquilo
% /dev/urandom
% março 2020

O vocabulário pra essa página

| word    | meaning                                      |
|---------|----------------------------------------------|
| ante    |  diferente, mudado, mudar                    |
| awen    | manter, ficar, aguentar, proteger, continuar |
| en      | e (combina sujeitos)                         |
| kalama  | som, barulho, fazer barulho                  |
| kulupu  | grupo, comunidade, sociedade                 |
| lape    | dormir, descansar                            |
| mute    | muito, mais, quantidade                      |
| ni      | isso, aquilo                                 |
| pakala  | quebrar, erro, (xingamento)                  |
| seli    |  calor,  reação química                      |

Antes de descobrirmos um novo tipo de palavras e nova gramática, vamos 
preencher alguns vazios.

A palavra "en" permite a combinação de vários sujeitos em uma frase:

> mi en sina li moku. -- Você e eu estamos comendo. 

Note que ela não é usada para combinar verbos ou objetos - a maneira de 
fazer isso foi explicada na [página 4](ptbr_4.html).

A palavra "mute" permite especificar se o sujeito (ou objeto) é singular 
ou plural.

> jan utala mute -- muitos guerreiros 

> mi mute -- nós

Aqui algumas frases de exemplo:

> jan lili mute li lape. -- As crianças estão dormindo. 

> kiwen suli li pakala e tomo lipu. -- Uma pedra grande danificou a 
> biblioteca ("casa de livros"). 

> mi pakala lili. -- Eu errei um pouquinho. 

> ilo sina li kalama mute ike. -- O seu instrumento está fazendo muito 
> barulho ruim. 

## A palavra "ni"

O uso mais simples da palavra "ni" é para significar "isso" ou "aquilo":

> kulupu ni li pona mute. --  Essa comunidade é muito boa. 

Porém, ela é muito mais poderosa que isso. A palavra "ni" também pode ser 
usada para criar frases mais complexas.

Ela pode ser usada para falar sobre o que outras pessoas falam (ou até 
citá-las, dependendo do contexto):

> jan lili li toki e ni: sina pona. --  A criança disse que você é bom. 

> ona li toki e ni: "toki! sina pona lukin." -- Ele disse "Olá! Você 
> parece bem." 

Ou pode ser usada para prover descrições mais detalhadas de sujeitos ou 
objetos.

> jan pali ni li pali e tomo mi: ona li jo e kiwen mute. -- O trabalhador 
> com muitas pedras construiu minha casa. ("Esse trabalhador construiu 
> minha casa: ele tem muitas pedras.")

## Diferenças em dialetos

> %info%
> Essa parte do documento descreve como certos cursos de toki pona se 
> diferem ao explicar certas ideias.

Não parece ter concordância em se "en" pode ser usado dentro de frases que 
usam a partícula "pi" (será explicada na página 9). Em adição, a própria 
Sonja Lang disse que usar "en" para combinar vários objetos não é 
"completamente errado", mas é inelegante.

## Exercícios

Agora, tente entender o significado dessas frases.

* kulupu sina li ante mute.
* jan ike li pakala e ilo mi.
* mi pali e tomo ni.
* jan utala pona mute li awen e kulupu ni.
* kulupu suli li awen, li suli e ona.

E tente traduzir as seguintes frases para toki pona.

* Comida quente é muito boa.
* Crianças dormindo não fazem barulhos.
* Os trabalhadores disseram que eles são fortes e durões.
* Você parece diferente.
* Essa casa preserva o calor.

[Respostas](ptbr_answers.html#5)

[Página anterior](ptbr_4.html) [Página inicial](ptbr_index.html) [Próxima página](ptbr_6.html)
