% Toki-Pona-Wörterbuch
% /dev/urandom
% Dezember 2020

Das hier ist eine alphabetisch sortierte Liste aller offiziellen Toki-Pona-Wörter, 
wie sie auf den Kursseiten 1 bis 12 auftauchen. Jedes Wort ist mit verschiedenen 
Bedeutungen aufgelistet, abhängig von der Position im Satzbau.

Die "Adjektiv"-Definition trifft sowohl auf Adjektive als auch auf Adverbien zu, 
da der einzige Unterschied in Toki Pona ist, ob es einem Nomen oder einem Verb 
folgt.

Ein Nomen kann immer als Adjektiv mit der Bedeutung "des [Nomens]" verwendet 
werden. Beispielsweise kann "moku soweli" je nach Kontext "Tierfutter" oder 
"Fleisch" bedeuten.

> %note%
> Das hier ist keine Kopie des Wörterbuchs, wie es im offiziellen Buch oder 
> in anderen Kursen gefunden wird. Manche Wörter können in anderen Satzteilen 
> verwendet werden, als hier aufgelistet sind. Es werden hier auch einige 
> unkonventionelle Bedeutungen gelistet, von denen manche nur in Verbindung 
> mit anderen Wörtern Sinn ergeben.
> 
> In Toki Pona ist es euch freigestellt, eigene Begriffe und alternative 
> Bedeutungen der Wörter in verschiedenen Zusammenhängen zu erstellen. Die 
> wichtigste Regel ist, sich verständlich zu machen.
>

---

#### a
* Funktionswort: (emotionaler Ausruf/Ausdruck, Betonung oder Bestätigung)

#### akesi
* Nomen: "nicht niedliches" Tier, Echse

#### ala
* Nomen: Nichts
* Adjektiv: nicht, nein, leer
* Zahl: Null

#### alasa
* Verb: jagen, sammeln
* Verb (unkonventionell): suchen

#### ale/ali
* Nomen: Gesamtheit, Universum
* Adjektiv: alles, jeder, reichlich
* Zahl: alles/unendlich (einfaches System), 100 (komplexes System)

#### anpa
* Nomen (alt/nicht pu): Unterteil
* Adjektiv: beugend, abwärts, bescheiden, demütig, abhängig
* Verb ohne Obj.: sich (nieder)beugen
* Verb mit Obj.: erobern, besiegen

#### ante
* Nomen: Unterschied, Veränderung, (unkonventionell) Version
* Adjektiv: anders, andere, verändert
* Verb: verändern

#### anu
* Funktionswort: oder

#### awen
* Nomen (unkonventionell): Stabilität, Sicherheit, Warten
* Adjektiv: erhalten, sicher, ausdauernd, belastbar, wartend, bleibend
* Verb: bewahren, bleiben, aushalten, schützen
* Hilfsverb: (etwas) fortsetzen

#### e
* Funktionswort: (spezifiziert ein Objekt)

#### en
* Funktionswort: und (kombiniert Subjekte)

#### esun
* Nomen: Handel, Transaktion, Tausch
* Adjektiv: tausch-/handelsbezogen, geschäftlich
* Verb: handeln, tauschen

#### ijo
* Nomen: Ding, Objekt, Materie
* Adjektiv: materiell, physisch

#### ike
* Nomen: Böse
* Adjektiv: böse, schlecht, komplex, unnötig
* Verb (unkonventionell): etwas schlimmer machen

#### ilo
* Nomen: Werkzeug, Maschine, Gerät
* Adjektiv: brauchbar, (unkonventionell) elektronisch, metallisch

#### insa
* Nomen: Innereien, Inhalt, Mitte, Bauch
* Adjektiv: mittig, innen, zwischen

#### jaki
* Nomen: Schmutz, Abfall
* Adjektiv: schmutzig, eklig, giftig, unsauber, unhygienisch
* Verb: etwas schmutzig machen

#### jan
* Nomen: Person, Volk, Leute, Menscheit, jemand
* Adjektiv: menschlich, persönlich

#### jelo
* Nomen/Adjektiv: (die Farbe) Gelb (und Schattierungen)
* Verb: etwas gelb färben

#### jo
* Nomen: (unkonventionell) Besitztum, Eigentum
* Verb: haben/tragen/beinhalten/halten

#### kala
* Nomen: Fisch, Meerestier, Meereslebewesen

#### kalama
* Nomen: Geräusch, Lärm
* Adjektiv: laut, lärmend, schallend
* Verb: Lärm machen, rezitieren, (ein Instrument) spielen

#### kama
* Nomen: Ereignis, Ankunft
* Adjektiv: ankommend, kommend, zukünftig
* Hilfsverb: werden, dabei sein (etwas) zu tun, erfolgreich sein in

#### kasi
* Nomen: Pflanze, Gras, Kraut, Blatt

#### ken
* Nomen: Fähigkeit, Möglichkeit, (unkonventionell) Recht, Freiheit
* Adjectiv (unkonventionell): fähig, möglich
* Hilfsverb: kann (etwas tun), darf (etwas tun)

#### kepeken
* Nomen: Nutzen, (unkonventionell) Übung
* Verb mit Obj.: etwas benutzen
* Präposition: (etwas) benutzend, mithilfe von

#### kili
* Nomen: Frucht, Gemüse, Pilz

#### kiwen
* Nomen: hartes Objekt, Metall, Stein, Festkörper
* Adjektiv: hart, metallisch, fest

#### ko
* Nomen: Pulver, Ton, Paste, halbfest

#### kon
* Nomen: Luft, Essenz, Geist, (unkonventionell) Gas
* Adjektiv: unsichtbar, flüchtig, (unkonventionell) gasförmig

#### kule
* Nomen: Farbe, (selten) Gender
* Adjektiv: bunt, gemalt
* Verb: bemalen

#### kulupu
* Nomen: Gruppe, Gemeinschaft, Firma, Gesellschaft, Nation, Stamm
* Adjektiv: gemeinschaftlich, gesellschaftlich

#### kute
* Nomen: Ohr, Gehör
* Adjektiv: ...-klingend
* Verb: hören, zuhören, gehorchen

#### la
* Funktionswort: (stellt Kontext voran)

#### lape
* Nomen: Schlaf, Ruhe
* Adjektiv: schlafend, erholsam
* Verb: schlafen, erholen

#### laso
* Nomen/Adjektiv: (die Farbe) Blau, Grün (und Schattierungen)
* Verb: etwas blau/grün färben

#### lawa
* Nomen: Kopf, Bewusstsein
* Adjektiv: Haupt-..., vorrangig, steuernd, herrschend
* Verb: leiten, kontrollieren, lenken, anführen, führen, besitzen, regieren

#### len
* Nomen: Stoff, Kleidung, Textil, Privatsphärenschutz
* Adjektiv: bekleidet, aus Stoff/Textilien gefertigt
* Verb: bekleiden, die Privatsphäre schützen

#### lete
* Nomen: Kälte
* Adjektiv: kalt, kühl, roh, ungekocht
* Verb: abkühlen

#### li
* Funktionswort: (zwischen Sunjekt und Verb/Adjektiv)

#### lili
* Nomen: Kleinheit
* Adjektiv: klein, wenige, jung
* Verb: schrumpfen

#### linja
* Nomen: langes biegsames Objekt, Schnur, Seil, Haar

#### lipu
* Nomen: flaches Objekt, Buch, Dokument, Papier, Seite, Aufzeichnung, Internetseite
* Adjektiv: flach, als lipu benutzt, lipu-ähnich, aus lipu

#### loje
* Nomen/Adjektiv: (die Farbe) Rot (und Schattierungen)
* Verb: etwas rot färben

#### lon
* Nomen: Wahrheit, Leben, Existenz
* Adjektiv: wahr, real, anwesend, existierend
* Verb ohne Objekt: ist wahr, existiert
* Präposition: in, auf, an

#### luka
* Nomen: Hand, Arm
* Zahl: 5 (komplexes System)

#### lukin
* Nomen: Auge, Sicht
* Adjektiv: ...-aussehend, visuell
* Verb: sehen, schauen, lesen
* Hilfsverb: (ver)suchen (etwas zu tun)

#### lupa
* Nomen: Loch, Tür, Öffnung, Fenster

#### ma
* Nomen: Erde, Land, Draußen, Territorium, Land

#### mama
* Nomen: Elternteil, Vorfahre, Schöpfer, Ursprung, Fürsorger
* Verb: erstellen, erziehen, betreuen

#### mani
* Nomen: Geld, großes Nutztier
* Adjektiv: (unkonventionell) reich

#### meli
* Nomen: Frau, Weibchen, Ehefrau
* Adjektiv: weiblich

#### mi
* Nomen: ich, wir
* Adjektiv: mein, unser

#### mije
* Nomen: Mann, Männchen, Ehemann
* Adjektiv: männlich

#### moku
* Nomen: Nahrung
* Adjektiv: essbar, nahrungs-
* Verb: essen, trinken, schlucken

#### moli
* Nomen: Tod
* Adjektiv: tot, sterbend
* Verb: töten

#### monsi
* Nomen: Rücken, Hinterteil, Rückteil, Hintern
* Adjektiv: zurück, hinten

#### mu
* (alle Tiergeräusche)

#### mun
* Nomen: Mond, Stern, Nachthimmelobjekt
* Adjektiv: lunar, stellar

#### musi
* Nomen: Spiel, Kunst
* Adjektiv: unterhaltsam, künstlerisch, lustig
* Verb: sich unterhalten, spielen, Spaß haben

#### mute
* Nomen: Anzahl
* Adjektiv: viel, mehr
* Zahl: 3 oder mehr (einfaches System), 20 (komplexes System)

#### nanpa
* Nomen: Nummer
* Adjektiv: -ste (Ordnungszahl-Angabe), mathematisch, numerisch, (unkonventionell) digital

#### nasa
* Adjektiv: seltsam, ungewöhnlich, merkwürdig, verrückt, betrunken

#### nasin
* Nomen: Weg, Straße, Art und Weise, Lehre, Brauch
* Adjektiv: wege-, richtlinienkonform
* Verb (unkonventionell): führen, den Weg zeigen

#### nena
* Nomen: Hügel, Berg, Knopf, Erhebung, Nase
* Adjektiv: hügelig, bergig, uneben

#### ni
* Nomen/Adjektiv: dies, das

#### nimi
* Nomen: Wort, Name

#### noka
* Nomen: Fuß, Bein, Unten, Unterteil, unter (smth)

#### o
* Funktionswort: (Ansprache, Befehle)

#### olin
* Nomen: Liebe, Mitgefühl, Zuneigung, Respekt
* Adjektiv: geliebt, lieblings-..., respektiert
* Verb: lieben, respektieren

#### ona
* Nomen: er, sie, es, sie
* Adjektiv: seine, ihre, seine, ihre

#### open
* Nomen: Start, Anfang
* Adjektiv: anfänglich, beginnend
* Verb: starten, öffnen, einschalten
* Hilfsverb: anfangen (etwas zu tun)

#### pakala
* Nomen: Schaden, Fehler
* Adjektiv: kaputt, defekt, falsch
* Verb: zerbrechen, Fehler machen
* Funktionswort: (allgemeiner Fluch)

#### pali
* Nomen: Arbeit, Werk
* Adjektiv: arbeitend, funktionierend
* Verb: (an etwas) arbeiten, machen, erstellen

#### palisa
* Nomen: langes festes Objekt, Ast, Stock, (unkonventionell) Länge
* Adjektiv: lang

#### pan
* Nomen: Brot, Getreide, Mais, Reis, Pizza

#### pana
* Adjektiv: (unkonventionell) gegeben, gesendet, freigegeben
* Verb: geben, senden, ausstrahlen, freilassen

#### pi
* Funktionswort: (gruppiert Adjektive um)

#### pilin
* Nomen: Herz, Gefühl, Sinn
* Adjektiv: gefühlt, ertastet
* Verb: fühlen, tasten, denken

#### pimeja
* Nomen: (die Farbe) Schwarz (und Schattierungen), Schatten
* Adjektiv: schwarz, dunkel
* Verb: etwas schwarz färben, einen Schatten werfen

#### pini
* Nomen: Ende, Schluss
* Adjektiv: endgültig, abgeschlossen, beendet, vorbei (mit tenpo)
* Verb: beenden, abschließen, schließen
* Hilfsverb: aufhören (etwas zu tun)

#### pipi
* Nomen: Insekt, Käfer

#### poka
* Nomen: Hüfte, Seite, Nähe
* Adjektiv: benachbart, nah, an der Seite

#### poki
* Nomen: Box, Behälter, Schüssel, Tasse, Schublade
* Verb (unkonventionell): einpacken, einkisten

#### pona
* Nomen: Güte, Einfachheit
* Adjektiv: gut, einfach, freundlich, friedlich
* Verb: verbessern, reparieren

#### pu
* Nomen: das offizielle Toki-Pona-Buch
* Adjektiv: wie im offiziellen Toki-Pona-Buch beschrieben
* Verb: mit dem offiziellen Toki-Pona-Buch interagieren

> %note%
> Das offizielle Toki-Pona-Buch definiert nur die Verb-Bedeutung des Wortes "pu".
> Manche Leute wollen es nur in der Verb-Version verwenden, während andere auch 
> die anderen benutzen.

#### sama
* Nomen: Ähnlichkeit, (jemandes) Geschwister
* Adjektiv: ähnlich, wie, geschwisterlich
* Präposition: as, like

#### seli
* Nomen: Hitze, Wärme, chemische Reaktion, Wärmequelle
* Adjektiv: warm, heiß
* Verb: wärmen

#### selo
* Nomen: äußere Form, äußere Schicht, Schale, Haut, Begrenzung
* Adjektiv: äußeres

#### seme
* Funktionswort: was? (für Fragen)

#### sewi
* Nomen: überliegender Bereich, Oben, oberer Teil, Himmel, Gott
* Adjektiv: hoch, über, göttlich, heilig

#### sijelo
* Nomen: Körper, physischer Zustand, Torso
* Adjektiv: physisch, körperlich

#### sike
* Nomen: Kreis, Kugel, Zyklus, Rad, (with tenpo) Jahr
* Adjektiv: rund, kreisförmig, kugelförmig, ein Jahr betreffend
* Verb: umkreisen

#### sin
* Nomen: Neuheit, Ergänzung, (unkonventionell) Update, Gewürz
* Adjektiv: neu, ergänzend, frisch, extra
* Verb: hinzufügen, aktualisieren

#### sina
* Nomen: du, Sie, ihr
* Adjektiv: dein, Ihr, ihr

#### sinpin
* Nomen: Gesicht, vorderster Teil, Front, Wand
* Adjektiv: das Gesicht betreffend, vorderst

#### sitelen
* Nomen: Symbol, Bild, Schrift
* Adjektiv: symbolisch, schriftlich, aufgezeichnet
* Verb: schreiben, zeichnen, aufzeichnen

#### sona
* Nomen: Wissen, Information
* Adjektiv: bekannt
* Verb: wissen
* Hilfsverb: wissen (wie man etwas tut)

#### soweli
* Nomen: Landsäugetier, Tier

#### suli
* Nomen: Größe, Großartigkeit
* Adjektiv: groß, schwer, hoch, groß, wichtig, erwachsen
* Verb: wachsen

#### suno
* Nomen: Sonne, Licht, Helligkeit, Lichtquelle
* Adjektiv: sonnig, hell
* Verb: leuchten, scheinen

#### supa
* Nomen: horizontale Oberfläche

#### suwi
* Nomen: (unkonventionell) Süßigkeiten, Aromen
* Adjektiv: süß, duftend, niedlich, bezaubernd

#### tan
* Nomen: Ursache, Grund, Herkunft
* Adjektiv: original-...
* Verb mit Objekt (unkonventionell): verursachen
* Präposition: von, wegen

#### taso
* Funktionswort (am Satzanfang): aber, jedoch
* Adjektiv: nur

#### tawa
* Nomen: Bewegung
* Adjektiv: bewegt
* Verb: bewegen
* Präposition: zu, für, aus der Perspektive von

#### telo
* Nomen/Adjektiv: Wasser, Flüssigkeit
* Adjektiv: nass, flüssig, fließend
* Verb: wässern, waschen

#### tenpo
* Nomen: Zeit, Moment, Anlass
* Adjektiv: zeitlich

#### toki
* Nomen: Rede, Gespräch, Sprache
* Adjektiv: verbal, gesprächig
* Verb: sprechen, reden, Sprache benutzen, denken

#### tomo
* Nomen: Haus, Gebäude, Struktur, Innenraum, Raum
* Adjektiv: innen

#### tu
* Zahl: 2
* Nomen: Trennung
* Adjektiv: geteilt
* Verb: trennen

> %warning%
> Die Verwendung von "tu" am Satzteilende wird normalerweise mit der Nummer 
> 2\ in Verbindung gebracht. Die Bedeutung "geteilt" wird im Regelfall in 
> einem Satz mit dem Funktionswort "li" spezifiziert:
>
> kulupu tu -- zwei Gesellschaften
>
> kulupu li tu. -- Die Gesellschaft ist geteilt.

#### unpa
* Nomen: Sex
* Adjektiv: sexuell
* Verb: sex haben (mit)

#### uta
* Nomen: Mund, Lippen
* Adjektiv: oral

#### utala
* Nomen: Kampf, Schlacht, Herausforderung, Krieg
* Adjektiv: aggressiv, kriegerisch
* Verb: kämpfen, streiten, herausfordern

#### walo
* Nomen: (die Farbe) Weiß (und Schattierungen)
* Adjektiv: weiß, hell
* Verb: etwas weiß färben

#### wan
* Zahl: 1
* Nomen: Teil (von etwas)
* Adjektiv: vereinigt, verheiratet
* Verb: vereinigen, heiraten

> %warning%
> Die Verwendung von "wan" am Satzteilende wird normalerweise mit der Nummer 
> 1\ in Verbindung gebracht. Die Bedeutung "vereinigt" wird im Regelfall in 
> einem Satz mit dem Funktionswort "li" spezifiziert:
>
> kulupu wan -- eine Gesellschaft
>
> kulupu li wan. -- Die Gesellschaft ist vereinigt.
>
> kulupu mute wan -- 21 Gesellschaften (komplexes Zahlensystem)
> 
> kulupu mute li wan -- Viele (oder 20) Gesellschaften sind vereinigt.

#### waso
* Nomen: Vogel, fliegendes Lebewesen

#### wawa
* Nomen: Stärke, Macht, Energie
* Adjektiv: stark, mächtig, energisch

#### weka
* Nomen: Abwesenheit, Ferne
* Adjektiv: abwesend, weg, entfernt
* Verb: entfernen, loswerden

#### wile
* Nomen: Wille, Bedürfnis, Wunsch
* Adjektiv: gewünscht, benötigt, erforderlich
* Verb: wollen
* Hilfsverb: etwas tun wollen

---

[Hauptseite](de_index.html)
