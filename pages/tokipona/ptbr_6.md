% toki pona página 6 - preposições e locais
% /dev/urandom
% março 2020

O vocabulário dessa página será dividido em dois grupos. Preposições:

| word    | meaning                                               |
|---------|-------------------------------------------------------|
| kepeken | usar, usando, com a ajuda de                          |
| lon     | em, dentro, em cima de, verdadeiro, presente, existir |
| sama    | igual, similar, como, irmão                           |
| tan     | de, por causa de, causa, razão                        |
| tawa    | para, mover, da perspectiva de                        |

E palavras regulares (que, nesse caso, se referem à locais):

| word    | meaning                                 |
|---------|-----------------------------------------|
| sewi    | acima, céu, divino, sagrado             |
| noka    | pé, perna, abaixo, embaixo              |
| poka    | quadril, lado, ao lado de, perto        |
| monsi   | costas, atrás, parte de trás, bunda     |
| sinpin  | rosto, de frente, primeiramente, parede |

Preposições são palavras que são conectadas a outras partes da frase 
para expressar um lugar ou tempo (mais sobre isso no futuro) ou detalhe 
específico sobre a ação.

Em toki pona, as palavras "kepeken", "lon", "sama", "tan" e "tawa" são 
usadas como preposições sendo adicionadas ao final da oração sem mais 
nenhuma partícula extra.

Aqui estão alguns exemplos dessas cinco palavras como preposições e palavras regulares:

> mi pona e tomo kepeken ilo mi. --  Eu estou consertando a casa usando minhas ferramentas.

> mi toki kepeken toki pona. --  Eu falo em toki pona. 

> sina kepeken e ilo sitelen. -- Você está usando uma ferramenta de escrita/desenho.  
> (caneta, lápis, pincel)

> mi lon tomo sina. -- Eu estou em sua casa.

> jan ike li kalama mute lon tomo lipu. -- Uma pessoa ruim está fazendo muito 
> barulho na biblioteca. 

> ona li toki e ijo lon. -- Ela fala a verdade.  ("fala de coisas verdadeiras")

> mi en sina li sama. -- Eu e você somos similares. 

> meli sama mi li pona. -- Minha irmã é boa.

> kiwen lili li sama lukin pipi. -- A pedrinha parece um inseto.  

> mi lape tan ni: mi jo ala e wawa. --  Eu durmo porque não tenho energia.

> mi tawa tan tomo mi. -- Estou saindo da minha casa.

> ona li awen lon tomo lipu. --  Eles ficaram na biblioteca.

> tomo tawa mi li pona. --  Meu carro é bom.  ("casa que move")

> mi tawa tomo moku. -- Eu estou indo para o restaurante.  ("casa de comida")

A palavra "tawa" também pode expressar perspectiva.

> sina pona tawa mi. -- Eu gosto de você.  ("Você é bom pra mim.")

> %warning%
> Já que "tawa" pode ser tanto um adjetivo quanto uma preposição, certas frases
> podem ser ambíguas. Por exemplo, "tomo tawa mi" pode significar "meu carro" 
> e "uma casa, da minha perspectiva". O significado especifico dependerá do 
> contexto.

E aqui estão alguns exemplos das palavras de local:

> waso mute li lon sewi. -- Muitos pássaros estão no céu. 

> mi awen lon tomo mi. --  Eu estou ficando na minha casa. 

> mi toki tawa jan sewi. -- Eu falo com uma (divinidade/anjo/alguém divino
> /possívelmente literalmente 'homem no céu') 

> mi tawa kepeken noka mi. -- Eu estou indo a pé. ("com minhas pernas") 

> kiwen lili li lon noka mi. -- Uma pedrinha está abaixo de mim. 

> mi tawa lon poka sina. -- Eu ando ao seu lado. 

> jan poka li ike tawa mi. -- Eu não gosto do meu vizinho. 

> poka mi li pakala. -- Meu quadril/lado está quebrado. 

> ona li lon monsi sina. -- Eles estão atrás de você. 

> jan utala mute li lon sinpin mi. -- Os guerreiros estão na minha frente. 

> lipu suli li lon sinpin ni. -- Um documento importante está nessa parede. 

> sinpin ona li pona lukin. -- O rosto dele é bonito. 

## Frases

Há duas maneiras diferentes de se dizer "adeus". Se você está saindo, é:

> mi tawa! --  Adeus! (literalmente: "Estou saindo.")

Se outra pessoa está saindo, é:
 
> tawa pona! --  Adeus!  (literalmente: "Bom movimento!")

Muitas palavras seguidas por "pona" são usadas como cumprimentos.

> moku pona! --  Bom apetite!  (literalmente: "Boa comida!")

> lape pona! --   Boa noite! / Bons sonhos!  (literalmente: "Bom sono!")

Também há uma frase que funciona como muitas expressões boas, desde "obrigado" até "fique em paz":

> pona tawa sina! (literalmente: "Bem pra você!")

## Diferenças em dialetos

> %info%
> Essa parte do documento descreve como certos cursos de toki pona se diferem ao 
> explicar certas ideias.

* O livro oficial usa "lon" com um significado abrangente, incluindo também "com". 
Por exemplo, "Eu falo em toki pona" é traduzido como "mi toki lon toki pona". 
Esse não parece ser um uso comum da palavra.

* O livro oficial usa "noka" para significar tanto "pé" quanto "abaixo". Esse é um 
uso relativamente novo, e outros cursos preferem usar "anpa" ao invés de "noka" 
para significar "abaixo" (usando "noka" para significar especificamente "perna" ou "pé"). 
A palavra "anpa" será apresentada na página 7.

* As vezes, palavras como "kepeken" e "tawa" podem ser usadas tanto como preposições 
("usando", "em direção à") e como verbos ("usar", "mover"). O livro oficial dá um exemplo 
para "kepeken", onde é usado como verbo:

> o kepeken ala ilo ike. --  Não use ferramentas ruins. 

Mas frequentemente na comunidade toki pona a partícula "e" também é adicionada, e "kepeken" 
é usado como verbo transitivo, não preposição:

> mi kepeken e ilo. -- Eu estou usando ferramentas. 

(Esse método era usado no curso "o kama sona e toki pona!".)

A diferença é que no primeiro exemplo, o objeto ("ilo ike") é segue diretamente a 
frase "kepeken ala",  e no segundo, "kepeken" é seguido por "e", como qualquer outro verbo.

Esse curso seguirá a segunda convenção, já que ela é menos ambígua e permite melhor 
gramática (por exemplo, é possível adicionar claramente mais modificadores após "kepeken"). 
Mas na maioria das frases, o uso ou não de "e" não deveria fazer o significado se tornar ambíguo.

## Exercícios

Agora, tente interpretar o significado dessas frases.

* ona li toki tawa mama mije ona kepeken ilo toki.
* moku suwi li ike tawa mi.
* ma mama mi li utala e ma poka.
* meli sama sina li jan pona mi.
* pipi lili li lon sinpin sina.

E tente traduzir as seguintes frases para toki pona.

* Você quebrou meu carro.
* O homem alimenta crianças. ("dá comida para")
* Eu não gosto desse chatroom. ("estrutura de falar")
* Eu consertei a casa por sua causa.
* Eu consertei a casa por sua causa.

[Respostas](ptbr_answers.html#p6)


[Página anterior](ptbr_5.html) [Página inicial](ptbr_index.html) [Próxima página](ptbr_7.html)
