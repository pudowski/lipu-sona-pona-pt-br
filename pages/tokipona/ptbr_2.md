% toki pona página 2 - adjetivos
% /dev/urandom
% março 2020

O vocabulário dessa página:

| palavra | significado                      |
|---------|----------------------------------|
| ala     | não, zero, negativo              |
| ale/ali | tudo, todos, universo            |
| utala   | luta, batalha, desafio           |
| wawa    | forte, poderoso                  |
| suwi    | doce, fofo, adorável             |
| jan     | pessoa, povo, humanidade         |
| mama    | pai, ancestral, criador, origem  |
| meli    | mulher, feminino                 |
| mije    | homem, masculino                 |
| moku    | comida, comer                    |


Para definir sujeitos e adjetivos mais claramente, você pode adicionar palavras 
extras como adjetivos. Em toki pona, um adjetivo que modifica um substantivo fica 
depois do subjetivo em si. Por exemplo:

> jan wawa -- pessoa forte 

Muitos dos substantivos estudados antes também podem exercer o papel de adjetivos. 
Por exemplo, os pronomes "mi", "sina" e "ona" podem servir como pronomes pessoais.

> mama mi -- minha mãe ou meu pai

> soweli sina -- teu animal 

> moku ona -- comida dele/a(s)

Além disso, adjetivos podem exercer o papel de substantivos:

> wawa sina -- tua/vossa força 

> suli ona -- a grandeza/tamanho dele(a) 

> %info%
> É importante notar a expressão "jan pona", que literalmente significa "boa
> pessoa", mas é abrangentemente (e oficialmente) usada como "amigo".

Múltiplos adjetivos podem ser adicionados juntos:

> soweli lili suwi -- animalzinho fofo 

Aqui estão algumas frases de exemplo que demonstram isso:

> mama mi li pona. - Meus pais são bons.

> kili suwi li moku pona. - Frutas doces são boa comida. 

> jan utala li wawa. - O guerreiro é forte. (literalmente "pessoa que luta")

> jan lili mi li suwi. - Minhas crianças são fofas. (literalmente "pessoa pequena")

> soweli lili li wawa ala. - Animaizinhos não são fortes. 

> %warning%
> É importante notar que a partícula "li" só é removida se o sujeito é 
> somente a palavra "mi" ou a palavra "sina". Se há adjetivos ao lado, 
> a partícula é usada.

> %info%
> Em adição à "mije" e "meli", algumas pessoas também usam a palavra
> "tonsi" para se referir à pessoas não-binárias ou outros que não se 
> encaixam em "homem" ou "mulher". Veja a [Página Extra 1](ptbr_x1.html) 
> para mais informação.

## Exercícios

Agora, tente entender o significado dessas frases.

* meli mi li pona.
* mije sina li suli.
* mama mije mi li wawa.
* soweli ale li pona.
* kili li moku suli.

E tente traduzir essas frases para toki pona.

* Minha esposa é adorável. 
* Minha esposa é adorável. 
* Meus amigos são seus amigos. 
* Teu filho é forte. 
* Teu filho é forte. 

[Respostas](ptbr_answers.html#p2)

[Página anterior](ptbr_1.html) [Página inicial](ptbr_index.html) [Próxima página](ptbr_3.html)
