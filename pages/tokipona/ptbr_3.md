% toki pona página 3 - verbos e objetos
% /dev/urandom
% março 2020

O vocabulário nesta página:

| palavra| significado                          |
|--------|--------------------------------------|
| e      | (especifica um objeto)               |
| ijo    | coisa, objeto                        |
| ilo    | ferramenta, máquina, dispositivo     |
| lipu   | livro, documento, papel              |
| lukin  | olho, olhar, ver, procurar fazer algo|
| olin   | amor, compaixão, afeto               |
| pali   | fazer, trabalhar, criar, trabalho    |
| pana   | dar, enviar, emitir                  |
| telo   | água, fluído, regar, limpar          |
| tomo   | casa, quarto, estrutura              |

Para adicionar um verbo à frase, use a seguinte estrutura:

> [substantivo] li [verbo] 

Por exemplo,

> mije li pali. - Um homem está trabalhando. ou O homem trabalha. 

> %warning%
> Não há infinitivo em toki pona, então todas as traduções feitas 
> no afirmativo funcionam também pro infinitivo e vice versa, mesmo 
> se não explicitamente colocadas aqui.

Tanto o substantivo quanto o verbo podem ter adjetivos após os mesmos. Se 
adicionado após um verbo, o adjetivo se torna um advérbio.

> jan wawa li pali pon. - Uma pessoa forte está trabalhando bem. 

> %warning%
> Não há maneira de determinar se uma palavra em uma frase assim é adjetivo 
> ou verbo. Por exemplo, a frase "mi moku" pode significar "Eu estou comendo" 
> ou "Eu sou comida".

> %info%
> Verbos não tem informação de tempo neles. Uma maneira de especificar tempo 
> será explicada em outra página.

Para adicionar um objeto, use a partícula "e" para criar a seguinte estrutura:

> [sujeito] li [verbo] e [objeto] 

> jan wawa li pali e tomo. - Uma pessoa forte está construindo uma casa. ou 
> A pessoa forte está trabalhando na casa. 

Objetos também podem ter adjetvos adicionados à eles.

> jan pali li pana e moku pona. - O trabalhador dá comida boa. 

Aqui estão algumas frases:

> jan pona mi li pona e ilo lukin. - Meu amigo está melhorando a ferramenta de 
> olhar.  ou  Meu amigo conserta ferramentas de olhar.  (ferramentas de olhar 
> podendo significar óculos, binóculos, microscópios, etc)

> mi telo e moku. - Eu limpo a comida. 

> mi olin e meli mi. - Eu amo minha esposa.  

> %warning%
> 
> Já que a palavra "lukin" em si descreve o ato de ver alguém, e não sua aparência 
> física, elogiar alguém nesse aspecto seria comumente expressado como:
>
> "sina pona lukin" - Você é bonito. ("Você é bom visualmente").
>
 
## Exercícios

Tente interpretar o significado dessas frases:

* jan lili li pana e telo lukin.
* ona li lukin e lipu.
* soweli ike li utala e meli.
* jan utala li moku e kili suli.
* soweli lili li moku e telo.
* mi telo e ijo suli.

E tente traduzir essas frases para toki pona:

* Ela ama todas as pessoas. 
* O banheiro ("casa de água") é bom. 
* Eu entrego documentos. 
* Um guerreiro do mal está observando a sua casa. 
* Minhas ferramentas estão funcionando bem.

[Respostas](ptbr_answers.html#p3)

[Página anterior](ptbr_2.html) [Página inicial](ptbr_index.html) [Próxima página](ptbr_4.html)
