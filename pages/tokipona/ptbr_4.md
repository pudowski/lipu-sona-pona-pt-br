% toki pona página 4 - ah não! mais vocabulário
% /dev/urandom
% março 2020

The vocabulary for this page:

| word    | meaning                                  |
|---------|------------------------------------------|
| jo      | ter, conter, carregar, segurar           |
| kala    | peixe, animal marinho, criatura aquática |
| kasi    | planta, grama, erva, folha               |
| pipi    | inseto                                   |
| sitelen | símbolo, imagem, escrever, desenhar      |
| toki    | falar, fala, linguagem                   |
| waso    | pássaro, criatura voadora                |
| ma      | terra, território                        |
| kiwen   | objeto duro, metal, pedra, sólido        |
| ko      | pó, argila, semi-sólido                  |

Essa página falará somente das dez novas palavras e de alguns simples conceitos. 

> jan pali li telo e kasi. - O trabalhador está regando as plantas. 

> jan wawa li jo e kiwen suli. -  A pessoa forte carrega as pedras grandes. 

> telo suli li jo e kala. -  O oceano tem peixes.  (literalmente: "A água grande tem peixes.")

> mi sitelen e toki sina. -  Estou escrevendo sua fala. 

> waso lili li moku e pipi. - O pássaro pequeno come insetos. 

> ma tomo mi li suli. - Minha cidade é grande.  (literalmente: "Meu território construído é grande.")

## Tópicos de conversa

> %warning%
> Não há consenso em qual dessas formas é mais correta, mas cada tem seus 
> próprios positivos e negativos. Tudo nesse tópico é sobre "diferenças 
> em dialetos", e as opiniões do autor estarão aqui.

Há duas maneiras comumente usadas de especificar o tópico de conversa quando usando 
a palavra "toki".

O mais simples, como usado em "o kama sona e toki pona!", é especificando o tópico 
como um adjetivo:

> ona li toki meli. --  Eles falam sobre mulheres. 

Porém, isso introduz incerteza quando adjetivos reais que se aplicam a "toki" são 
introduzidos. "toki ike" quer dizer "falar mal" ou "falar sobre mal"?

O livro oficial não é claro sobre isso, mas usa "toki e ijo" para significar 
"comunicar coisas" e "toki wawa" como "atestar" ("falar fortemente"), ao invés de 
"falar sobre força".

A versão extendida desse método, comumente usada na comunidade de toki pona, 
é usar o tópico como um objeto:

> sina toki e kala. --  Você fala sobre peixes. 

Enquanto isso é considerado por alguns como um uso inconvencional da partícula 
"e", esse método é menos ambíguo e mais flexível. Para maior clareza, essa 
opção será usada durante o curso.

## Frases de exemplo

E aqui vão algumas frases interessantes.

> jan pali li toki utala e tomo mi. - O trabalhador critica a minha casa. 
> (literalmente: "fala de maneira lutadora sobre")

> ona li toki ike e jan pona mi. -  Eles insultam meus amigos.  ou  
> Ela fala mal de meus amigos. 

Você pode colocar vários verbos e vários objetos em uma única frase 
adicionando mais partículas "li" ou "e" seguido por seus verbos ou objetos.

> meli li toki e soweli, e waso. - A mulher fala de animais terrestres e 
> pássaros. 

> jan pali li pona e ilo, li lukin e lipu. - A trabalhadora conserta o 
> dispositivo e lê o documento.  (literalmente: "olha o documento.")

## Frases

A palavra "toki", quando usada sozinha, é um cumprimento comum:

> toki! -- Olá

## Diferenças em dialetos

> %info%
> Essa parte do documento descreve como certos cursos de toki pona se 
> diferem ao explicar certas ideias.

Se o sujeito é "mi" ou "sina" (e, portanto, não é acompanhado da partícula 
"li"), você pode fazer uma de duas coisas para adicionar um verbo extra.

* O livro oficial ("pu") sugere que você simplesmente duplique a frase:

> mi pali. mi moku. - Eu trabalho e como. 

* A série de vídeos "12 days of sona pi toki pona" sugere adicionar uma 
partícula "li" (assim como "o kama sona e toki pona!"):

> mi pali, li moku. - Eu trabalho e como. 

## Exercícios

Agora, tente interpretar o significado dessas frases.

* mi moku ala e soweli.
* jan pona sina li toki e ma, e telo.
* jan suli li lukin e ma tomo, li sitelen e ijo.
* ma li jo e kasi ike.
* pipi lili li suli, li pona.

E tente trauzir as seguites frases para toki pona.

* Sua cidade não tem trabalhadores.
* Meu marido não trabalha, só come e briga.
* Minha terra natal é grande.
* Tua pintura é bonita.
* Meu amigo tem peixe e fruta e faz comida boa.

[Respostas](ptbr_answers.html#p4)

[Página anterior](ptbr_3.html) [Página inicial](ptbr_index.html) [Próxima página](ptbr_5.html)
