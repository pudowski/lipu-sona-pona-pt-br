% rnd's website
% /dev/urandom
% date unknown

<style>
.header h3, .titlesep {
	display: none;
}
</style>

# Hello and welcome!

Welcome to my little website! I'm running it mostly as an experiment in static
website generation, but also to host some content that others might find
interesting.

* [about me](about_me.html)

* [lipu sona pona - a toki pona course](tokipona/)

* [blog thingy](blog/)

* [rnd's games](games.html)

* [pixel art fonts](pixel_art_fonts.html) **NEW**

* [building a static website](static.html)

* [website faq](faq.html)

* [random links](links.html)

* [my banners](banners.html)

> %warning%
> This is yet another of those "under construction" messages you'd see on
> pre-Web 2.0 websites.

