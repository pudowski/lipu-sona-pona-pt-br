% my website's banners
% /dev/urandom
% november 2020

# Banners

If you feel like it, you can put up these 88x31 banners linking to my website:

## rnd's corner

[![rnd's
corner](https://rnd.neocities.org/banners/my_banner.gif)](https://rnd.neocities.org)

HTML code:
```
<a href="https://rnd.neocities.org"><img src="https://rnd.neocities.org/banners/my_banner.gif" alt="rnd's
corner"></a>
```
## lipu sona pona

[![lipu sona
pona](https://rnd.neocities.org/banners/tokipona_banner.gif)](https://rnd.neocities.org/tokipona)

HTML code:
```
<a href="https://rnd.neocities.org/tokipona"><img src="https://rnd.neocities.org/banners/tokipona_banner.gif" alt="toki pona" title="learn a minimalist language of 120 words with 'lipu sona pona'></a>
```
