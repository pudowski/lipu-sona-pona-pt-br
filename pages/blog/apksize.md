% alpine linux package size list script
% /dev/urandom
% august 2020

This script will list the sizes of all packages in your Alpine Linux install:

    #!/bin/bash
      
    PACKAGES=`apk info -e -s \*`
    
    a=`echo "$PACKAGES" | awk 'NR % 3 == 1' | cut -d ' ' -f 1`
    PKGNAMES=($a)
    a=`echo "$PACKAGES" | awk 'NR % 3 == 2'`
    PKGSIZES=($a)
    
    for i in ${!PKGNAMES[@]}; do
            echo -e "${PKGSIZES[$i]} - ${PKGNAMES[$i]}"
    done

Run it with `./apksize.sh | sort -n | uniq` to get a properly-sorted and
deduplicated list.
