% phantasy star online: blue burst on wine
% /dev/urandom
% august 2020

When I first launched "Phantasy Star Online: Blue Burst" -- more
specifically, the client provided by the
[Ephinea](https://ephinea.pioneer2.net/) private server -- on wine, it worked
fine, but with one minor error:

the transparent scrolling textures that the game often uses to display computer
monitors were invisible (see screenshots
[here](https://twitter.com/_dev_urandom_/status/1289888125334859778)).

I tried using different versions of Wine, different settings in Lutris, and more
to no avail, until I discovered [this
thread](https://www.pioneer2.net/community/threads/make-psobb-pretty-and-run-smoothly.18372/)
on the Ephinea forums. It describes a utility called "dgVoodoo" that can rewire an
old Direct3D 8/9 program to use the APIs of DirectX 11. Given that, in current
versions of Wine, most effort is being spent on DirectX 11, trying it out seemed
like a good idea.

However, when I first installed it, it resulted in an even worse outcome -- the
game launched with a permanent black screen instead of the graphics. Luckily
enough, it turned out that the solution to _that_ was even simpler: to turn on
the "Enable DXVK/VKD3D" option in Lutris' settings for the game. After this, the
game ran perfectly, and the previously-missing textures returned (see [this
excited tweet](https://twitter.com/_dev_urandom_/status/1289943626827460613) for
a screenshot with confirmation).
