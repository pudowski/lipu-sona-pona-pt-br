% profiling utility hints
% /dev/urandom
% may 2020

# gmon and gperf hints

* compile with `-pg` in both `CFLAGS/CXXFLAGS` and `LDFLAGS`

* use env variable `GMON_OUT_PREFIX` to make programs output to 
  `$GMON_OUT_PREFIX.$pid` instead of `gmon.out`

* despite warnings, usable on multithreaded programs (all threads'
  times are added up), even if you can't tell individual threads apart

# using linux's perf

* record: `perf record -g [-o outputfile] program`

* view data: `perf report [-i inputfile]`

When a program is running with MPI, its rank is stored in the `$PMI_RANK` env
variable.

