% trying to do assembly-level debugging in gdb
% /dev/urandom
% november 2020

GDB seems to have been clearly made with source-level debugging in mind, the
kind where the source code of the program is available. Sometimes this is not
the case, or you just don't want to bother looking for the source code. Here
I'll put a bunch of useful commands.

* `layout asm`: enable TUI mode and show assembly instructions

* `layout regs`: enable TUI mode and show CPU registers

* `set disassembly-flavor intel`: use Intel-like disassembly, which is more
  familiar to those writing or debugging assembly code on Windows

* `Ctrl+P`, `Ctrl+N`: serve as alternatives to "up" and "down" when the active
  TUI window is not the command window

* `Ctrl+X, O`: switch the active TUI window

* `Ctrl+X, (A or Shift+A or Ctrl+A)`: toggle TUI mode

* `x/5i $pc`: check out 5 instructions, starting at the program counter

* `ni` (`nexti`) and `si` (`stepi`): next/step, but on an instruction level

* `b *0x<addr>`: breakpoint on a specific address

* `info registers`: look at CPU registers

* `info frame`: look at current stack frame, potentially useful for finding
  function arguments and local vars

* `x/8g $sp`: look at (64-bit words) in the stack
