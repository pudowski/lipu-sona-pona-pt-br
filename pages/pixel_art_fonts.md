% pixel art fonts
% /dev/urandom
% november 2020

This is the page where I'll be hosting all the pixel art fonts which I drew.

These may be in the following formats:

 * **BDF** (Linux/X11 bitmap font, monochrome, can support Unicode)

 * **Bitmap Font Writer** (multi-color, ASCII only, can be used with the Grafx2
   pixel art editor)

 * **FON** (Windows bitmap font, monochrome, only supports one of Windows'
   predefined character sets)

 * **raw image** (these are typically monospaced, but can sometimes be
   variable-width)

## License

Everyone is free to use these bitmap fonts for any purpose.

# Fonts

## Benny

Letter set: ASCII only

Format: Bitmap Font Writer (GIF image)

This one started as an attempt to imitate certain lowercase letter shapes used in the font
"Benjamin", but is very different from it otherwise. For one, its uppercase
letters are not as large and look quite differently.

![Benny font](/fonts/benny.gif)

## Marveldon, Marvelscript

Letter set: ASCII, French diacritics, German sharp S (ß)

Format: raw image

This is a series of variable width fonts I drew up for
[LuigiBlood](https://twitter.com/LuigiBlood/)'s translation of the Super Famicom
video game "Marvelous: Another Treasure Island". The font uses the SNES
high-resolution mode, and therefore all of its pixels should be twice as narrow.

The initial font was called "Marveldon" and was inspired by Clarendon -- since
the game's design made me think of classic American stories written or set
around the Wild West period, I decided to go with a Wild West-style font. It was
considered to be too wide, however, and I made several adjustments to its width
until eventually redrawing it in an italicized manner, calling the new one
"Marvelscript".

The width of each letter is determined by how wide the actual character
(including the outline, which in this case is blue) is.

![Marveldon font, version 1](/fonts/marveldon.gif)

![Marveldon font, version 2](/fonts/marveldon.gif)

![Marveldon font, version 3](/fonts/marveldon2x.gif)

![Marvelscript font](/fonts/marvelscript.gif)

## Year 2000

Letter set: ASCII, Russian Cyrillic (excluding Ё)

Format: raw image

This is a rather simple monospaced font of the retro-futuristic "computer"
style. 

![Year 2000 font](/fonts/year2000.gif)

## Service

Letter set: Latin, Greek, Russian Cyrillic, Katakana

Format: raw image

This is an attempt to imitate the design of the SEGA logo and extend it to other
characters and scripts.

![Service font](/fonts/service.gif)

## Petal Crash Cyrillic

Letter Set: Russian/Belarusian/Ukrainian Cyrillic

Format: raw image

This is just a Cyrillic version of the monospaced 8x8 font used in the game ["Petal
Crash"](https://twitter.com/PetalCrash). The idea is that anyone who wants to
translate the game into Russian, Ukrainian or Belarusian is free to use this
font.

![Petal Crash Cyrillic font](/fonts/petal_crash_cyrillic.gif)

