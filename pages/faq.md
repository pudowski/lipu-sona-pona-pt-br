% website faq
% /dev/urandom
% july 2020

## What's up with the "internet time" counter in the bottom left?

This is a [Swatch Internet
Time](https://en.wikipedia.org/wiki/Swatch_Internet_Time) meter. It displays
time of day, except instead of hours and minutes, the day is divided into 1000
"beats". Among other things, this measurement system was used in the 2000 Sega
Dreamcast game *Phantasy Star Online* (and its later re-releases), in which it
was displayed in a similar manner on in-game menus.
